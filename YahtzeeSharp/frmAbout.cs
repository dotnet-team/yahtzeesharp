/*************************************************************************
*                      frmAbout.cs                                       *
*                                                                        *
* Copyright (C) 2009 Andrew York <yahtzeesharp@brdstudio.net>            *
*                                                                        *
*************************************************************************/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

using Gdk;
using Gtk;
using System;
using System.Configuration;
using System.Reflection;

namespace YahtzeeSharp
{
	
	public partial class frmAbout : Gtk.Dialog
	{
		private Gdk.Cursor  cursorhand= new Gdk.Cursor(Gdk.CursorType.Hand2); 
		private Gdk.Cursor  cursordefault = null;		
		public frmAbout()
		{
			Build();
			
			this.lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
			
			LinkButton lbnLicense = new LinkButton("GPL Version 3","GPL Version 3");
			lbnLicense.Entered += Button_MouseHover;
			lbnLicense.Clicked += lbnLicense_Clicked;
			algLicense.Add(lbnLicense);
			
			LinkButton lbnAuthor = new LinkButton("Andrew York <yahtzeesharp@brdstudio.net>","Andrew York <yahtzeesharp@brdstudio.net>");
			lbnAuthor.Entered += Button_MouseHover;
			lbnAuthor.Clicked += lbnAuthor_Clicked;
			algAuthor.Add(lbnAuthor);
			
			LinkButton lbnProjectPage = new LinkButton("https://sourceforge.net/projects/yahtzeesharp/","https://sourceforge.net/projects/yahtzeesharp/");
			lbnProjectPage.Entered += Button_MouseHover;
			lbnProjectPage.Clicked += lbnProjectPage_Clicked;
			algProjectPage.Add(lbnProjectPage);
			
			LinkButton lbnWebSite = new LinkButton("http://www.brdstudio.net/yahtzeesharp/","http://www.brdstudio.net/yahtzeesharp/");
			lbnWebSite.Entered += Button_MouseHover;
			lbnWebSite.Clicked += lbnWebSite_Clicked;
			algWebSite.Add(lbnWebSite);
			
			InsertText(textview1.Buffer);
			this.ShowAll();
		}

		protected virtual void lbnLicense_Clicked(object sender, System.EventArgs e)
		{
			frmLicense fm = new frmLicense();
            fm.Run();
			fm.Destroy();
		}

		protected virtual void lbnAuthor_Clicked(object sender, System.EventArgs e)
		{
			string toEmail = "yahtzeesharp@brdstudio.net";
			string subject = "YahtzeeSharp";
			string body = "";

			string message = string.Format("mailto:{0}?subject={1}&body={2}", toEmail, subject, body);
			System.Diagnostics.Process.Start(message);
			
		}

		protected virtual void lbnProjectPage_Clicked(object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start("https://sourceforge.net/projects/yahtzeesharp/");
		}

		protected virtual void lbnWebSite_Clicked(object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start("http://www.brdstudio.net/yahtzeesharp/");
		}
		
		protected virtual void OnBtnOkClicked(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		protected virtual void OnCmdBitRockClicked (object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start(@"http://bitrock.com/products_installbuilder_overview.html");
		}

		protected virtual void OnCmdSharpDevelopClicked (object sender, System.EventArgs e)
		{
		
			System.Diagnostics.Process.Start(@"http://www.icsharpcode.net/OpenSource/SD/");
		}

		protected virtual void OnCmdMonoClicked (object sender, System.EventArgs e)
		{
		
			System.Diagnostics.Process.Start(@"http://www.mono-project.com");
		}

		protected virtual void OnCmdEntered (object sender, System.EventArgs e)
		{
			Gtk.Button mButton = (Gtk.Button)sender;
			 this.GdkWindow.Cursor = cursorhand;
   			mButton.Show();
   			this.GdkWindow.Display.Sync(); 
		}

		protected virtual void OnCmdLeft (object sender, System.EventArgs e)
		{
		Gtk.Button mButton = (Gtk.Button)sender;
			  this.GdkWindow.Cursor = cursordefault;
   			mButton.Show();
   			this.GdkWindow.Display.Sync(); 
		
		}
		
		protected virtual void Button_MouseHover(object sender, System.EventArgs e)
		{
			Gtk.Button mButton = (Gtk.Button)sender;
			mButton.State = Gtk.StateType.Normal;
		}
		
		private void InsertText(Gtk.TextBuffer buffer)
		{
			Common.BufferTags.GetTags(buffer);
			Gtk.TextIter insertIter = buffer.StartIter;
			buffer.InsertWithTagsByName(ref insertIter, "An open source Yahtzee like game written in C#\n", "heading");
			buffer.InsertWithTagsByName(ref insertIter, "Intended to be laid out exactly like the game we all used to play around the kitchen table. YahtzeeSharp was build using the Mono .NET Framework, Monodevelop and SharpDevelop development environments and distributed using BitRock Install Builder.\n\n", "monospaced");
			buffer.InsertWithTagsByName(ref insertIter, "Special Thanks To:\n", "bold");
			buffer.InsertWithTagsByName(ref insertIter, "* ", "heading_left");
			buffer.InsertWithTagsByName(ref insertIter, "My wife Charlene best wife ever.\n", "monospaced");
			buffer.InsertWithTagsByName(ref insertIter, "* ", "heading_left");
			buffer.InsertWithTagsByName(ref insertIter, "BitRock for supporting open source software with their outstanding installing packages\n", "monospaced");
			buffer.InsertWithTagsByName(ref insertIter, "* ", "heading_left");
			buffer.InsertWithTagsByName(ref insertIter, "Stefan Ebner for his patient guidance in helping me understand GNU standardization, even though I still have much to learn.\n", "monospaced");
			buffer.InsertWithTagsByName(ref insertIter, "* ", "heading_left");
			buffer.InsertWithTagsByName(ref insertIter, "All the encouragement and friendship I have received from all my brothers and sisters at Freedom Fighters.\n", "monospaced");
			buffer.InsertWithTagsByName(ref insertIter, "* ", "heading_left");
			buffer.InsertWithTagsByName(ref insertIter, "All my co-workers at CSI for listening to me talk about Mono/Gtk# programming even though they probably don't care and may think I've lost my mind for coding at for free home after doing it all day with them.\n", "monospaced");
		}
	}
	
}
