/*************************************************************************
*                      frmError.cs                                       *
*                                                                        *
* Copyright (C) 2009 Andrew York <yahtzeesharp@brdstudio.net>         *
*                                                                        *
*************************************************************************/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

using System;
using System.Configuration;

namespace YahtzeeSharp
{
	
	
	public partial class frmError : Gtk.Dialog
	{
		
		public frmError(string mError)
		{
			Build();
			System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly ();
			 this.txtErrorDetails.Buffer.Text = asm.GetName().Version.ToString()  + "\n\r" + "-------------------------------------------------------\n\r" + mError;
		}

		protected virtual void OnBtnYesEntered (object sender, System.EventArgs e)
		{
			string toEmail = "yahtzeesharp@brdstudio.net";
			string subject = "YahtzeeSharp bug report";
			string body = txtErrorDetails.Buffer.Text;

			string message = string.Format("mailto:{0}?subject={1}&body={2}", toEmail, subject, body);
			System.Diagnostics.Process.Start(message);
			this.Hide();
		}

		protected virtual void OnBtnNoClicked (object sender, System.EventArgs e)
		{
			this.Hide();
		}
	}
}
