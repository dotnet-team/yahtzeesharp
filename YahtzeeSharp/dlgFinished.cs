/*************************************************************************
*                      dlgFinished.cs                                    *
*                                                                        *
* Copyright (C) 2009 Andrew York <yahtzeesharp@brdstudio.net>            *
*                                                                        *
*************************************************************************/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

using System;

namespace YahtzeeSharp
{
	
	/// <summary>
		///  this is the dialog box that displays when the game is finished
		/// showing the final score
		/// </summary>
	public partial class dlgFinished : Gtk.Dialog
	{
		
		
		public dlgFinished()
		{
			this.Build();
			this.cmdYes.GrabFocus();
		}

		///<summary>
		/// executed when a new game is not wanted
		///</summary>
		protected virtual void OnCmdNoClicked (object sender, System.EventArgs e)
		{
			this.Dispose();
			this.Visible = false;
		}

		///<summary>
		/// executed when a new game is wanted
		///</summary>
		protected virtual void OnCmdYesClicked (object sender, System.EventArgs e)
		{
			this.Dispose();
			this.Visible = false;
		}
		
		///<summary>
		/// Public method to se the player name for display 
		///</summary>
		public void SetPlayerName(string strPlayerName)
		{
			this.lblPlayerName.Text = strPlayerName;
		}
		
		///<summary>
		/// Method to set the players score for display
		///</summary>
		public void SetScore(string strScore)
		{
			this.lblScore.Text = strScore;
		}
	}
}
