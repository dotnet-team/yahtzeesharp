// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.42
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace YahtzeeSharp {
    
    
    public partial class dlgOpen {
        
        private Gtk.VBox vbox2;
        
        private Gtk.HBox hbox1;
        
        private Gtk.Label lblfilter;
        
        private Gtk.Entry txtFilter;
        
        private Gtk.Button btnClearFilter;
        
        private Gtk.CheckButton cbxCompletedGames;
        
        private Gtk.ScrolledWindow scrolledwindow1;
        
        private Gtk.TreeView gvOpen;
        
        private Gtk.HSeparator hseparator2;
        
        private Gtk.Button cmdOpen;
        
        private Gtk.Button cmdCancel;
        
        private Gtk.Button cmdDelete;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget YahtzeeSharp.dlgOpen
            this.Events = ((Gdk.EventMask)(256));
            this.Name = "YahtzeeSharp.dlgOpen";
            this.Title = Mono.Unix.Catalog.GetString("Open");
            this.WindowPosition = ((Gtk.WindowPosition)(4));
            this.BorderWidth = ((uint)(4));
            this.HasSeparator = false;
            // Internal child YahtzeeSharp.dlgOpen.VBox
            Gtk.VBox w1 = this.VBox;
            w1.Events = ((Gdk.EventMask)(256));
            w1.Name = "dialog_VBox";
            w1.BorderWidth = ((uint)(2));
            // Container child dialog_VBox.Gtk.Box+BoxChild
            this.vbox2 = new Gtk.VBox();
            this.vbox2.Name = "vbox2";
            // Container child vbox2.Gtk.Box+BoxChild
            this.hbox1 = new Gtk.HBox();
            this.hbox1.Name = "hbox1";
            // Container child hbox1.Gtk.Box+BoxChild
            this.lblfilter = new Gtk.Label();
            this.lblfilter.WidthRequest = 48;
            this.lblfilter.Name = "lblfilter";
            this.lblfilter.LabelProp = "Filter";
            this.hbox1.Add(this.lblfilter);
            Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.hbox1[this.lblfilter]));
            w2.Position = 0;
            w2.Expand = false;
            w2.Fill = false;
            w2.Padding = ((uint)(8));
            // Container child hbox1.Gtk.Box+BoxChild
            this.txtFilter = new Gtk.Entry();
            this.txtFilter.CanFocus = true;
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.IsEditable = true;
            this.txtFilter.InvisibleChar = '●';
            this.hbox1.Add(this.txtFilter);
            Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.hbox1[this.txtFilter]));
            w3.Position = 1;
            w3.Padding = ((uint)(3));
            // Container child hbox1.Gtk.Box+BoxChild
            this.btnClearFilter = new Gtk.Button();
            this.btnClearFilter.CanFocus = true;
            this.btnClearFilter.Name = "btnClearFilter";
            this.btnClearFilter.Label = "Clear Filter";
            this.hbox1.Add(this.btnClearFilter);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.hbox1[this.btnClearFilter]));
            w4.Position = 2;
            w4.Expand = false;
            w4.Fill = false;
            w4.Padding = ((uint)(14));
            // Container child hbox1.Gtk.Box+BoxChild
            this.cbxCompletedGames = new Gtk.CheckButton();
            this.cbxCompletedGames.CanFocus = true;
            this.cbxCompletedGames.Name = "cbxCompletedGames";
            this.cbxCompletedGames.Label = Mono.Unix.Catalog.GetString("Don't Show Completed Games");
            this.cbxCompletedGames.Active = true;
            this.cbxCompletedGames.DrawIndicator = true;
            this.cbxCompletedGames.UseUnderline = true;
            this.hbox1.Add(this.cbxCompletedGames);
            Gtk.Box.BoxChild w5 = ((Gtk.Box.BoxChild)(this.hbox1[this.cbxCompletedGames]));
            w5.Position = 3;
            this.vbox2.Add(this.hbox1);
            Gtk.Box.BoxChild w6 = ((Gtk.Box.BoxChild)(this.vbox2[this.hbox1]));
            w6.Position = 0;
            w6.Expand = false;
            w6.Fill = false;
            w6.Padding = ((uint)(7));
            // Container child vbox2.Gtk.Box+BoxChild
            this.scrolledwindow1 = new Gtk.ScrolledWindow();
            this.scrolledwindow1.CanFocus = true;
            this.scrolledwindow1.Name = "scrolledwindow1";
            this.scrolledwindow1.ShadowType = ((Gtk.ShadowType)(1));
            // Container child scrolledwindow1.Gtk.Container+ContainerChild
            this.gvOpen = new Gtk.TreeView();
            this.gvOpen.CanFocus = true;
            this.gvOpen.Name = "gvOpen";
            this.gvOpen.RulesHint = true;
            this.scrolledwindow1.Add(this.gvOpen);
            this.vbox2.Add(this.scrolledwindow1);
            Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.vbox2[this.scrolledwindow1]));
            w8.Position = 1;
            w1.Add(this.vbox2);
            Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(w1[this.vbox2]));
            w9.Position = 0;
            // Container child dialog_VBox.Gtk.Box+BoxChild
            this.hseparator2 = new Gtk.HSeparator();
            this.hseparator2.Name = "hseparator2";
            w1.Add(this.hseparator2);
            Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(w1[this.hseparator2]));
            w10.PackType = ((Gtk.PackType)(1));
            w10.Position = 2;
            w10.Expand = false;
            w10.Fill = false;
            w10.Padding = ((uint)(5));
            // Internal child YahtzeeSharp.dlgOpen.ActionArea
            Gtk.HButtonBox w11 = this.ActionArea;
            w11.Name = "GtkDialog_ActionArea";
            w11.Spacing = 6;
            w11.BorderWidth = ((uint)(5));
            w11.LayoutStyle = ((Gtk.ButtonBoxStyle)(1));
            // Container child GtkDialog_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.cmdOpen = new Gtk.Button();
            this.cmdOpen.CanFocus = true;
            this.cmdOpen.Name = "cmdOpen";
            this.cmdOpen.UseStock = true;
            this.cmdOpen.UseUnderline = true;
            this.cmdOpen.Label = "gtk-open";
            this.AddActionWidget(this.cmdOpen, 0);
            Gtk.ButtonBox.ButtonBoxChild w12 = ((Gtk.ButtonBox.ButtonBoxChild)(w11[this.cmdOpen]));
            w12.Expand = false;
            w12.Fill = false;
            // Container child GtkDialog_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.cmdCancel = new Gtk.Button();
            this.cmdCancel.CanFocus = true;
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.UseStock = true;
            this.cmdCancel.UseUnderline = true;
            this.cmdCancel.Label = "gtk-close";
            this.AddActionWidget(this.cmdCancel, -7);
            Gtk.ButtonBox.ButtonBoxChild w13 = ((Gtk.ButtonBox.ButtonBoxChild)(w11[this.cmdCancel]));
            w13.Position = 1;
            w13.Expand = false;
            w13.Fill = false;
            // Container child GtkDialog_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.cmdDelete = new Gtk.Button();
            this.cmdDelete.CanFocus = true;
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.UseStock = true;
            this.cmdDelete.UseUnderline = true;
            this.cmdDelete.Label = "gtk-delete";
            this.AddActionWidget(this.cmdDelete, 0);
            Gtk.ButtonBox.ButtonBoxChild w14 = ((Gtk.ButtonBox.ButtonBoxChild)(w11[this.cmdDelete]));
            w14.Position = 2;
            w14.Expand = false;
            w14.Fill = false;
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 651;
            this.DefaultHeight = 382;
            this.Show();
            this.txtFilter.Changed += new System.EventHandler(this.OnTxtFilterChanged);
            this.btnClearFilter.Clicked += new System.EventHandler(this.OnBtnClearFilterClicked);
            this.cbxCompletedGames.Clicked += new System.EventHandler(this.OnCbxCompletedGamesClicked);
            this.gvOpen.RowActivated += new Gtk.RowActivatedHandler(this.OnGvOpenRowActivated);
            this.cmdOpen.Clicked += new System.EventHandler(this.OnBtnOpenClicked);
            this.cmdCancel.Clicked += new System.EventHandler(this.OnBtnCloseClicked);
            this.cmdDelete.Clicked += new System.EventHandler(this.OnBtnDeleteClicked);
        }
    }
}
