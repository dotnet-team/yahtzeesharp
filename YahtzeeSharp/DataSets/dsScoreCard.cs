/*************************************************************************
*                      dsScoreCard.cs                                    *
*                                                                        *
* Copyright (C) 2009 Andrew York <yahtzeesharp@brdstudio.net>            *
*                                                                        *
*************************************************************************/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

using System;
using System.Data;

namespace YahtzeeSharp.DataSets
{
	/// <summary>
	/// Description of dsScoreCard.
	/// </summary>
	public class dsScoreCard : DataSet
	{
		
		public dsScoreCard()
		{
			this.DataSetName = "dsScoreCard";
			DataTable dt = new DataTable("dtScoreCard");
			DataColumn[] dc = new DataColumn[] { 
												new DataColumn("RowTitle",Type.GetType("System.String")),
												new DataColumn("Game1",Type.GetType("System.Int32")),
												new DataColumn("Game2",Type.GetType("System.Int32")),
												new DataColumn("Game3",Type.GetType("System.Int32")),
												new DataColumn("Game4",Type.GetType("System.Int32")),
												new DataColumn("Game5",Type.GetType("System.Int32"))
											};
			
			dc[1].AllowDBNull = true;
			dc[2].AllowDBNull = true;
			dc[3].AllowDBNull = true;
			dc[4].AllowDBNull = true;
			dc[5].AllowDBNull = true;
			
			dt.Columns.AddRange(dc);
			dt.PrimaryKey = new DataColumn[]{dc[0]};
			this.Tables.Add(dt);
			InitScoreCard();
		}
		
		private void InitScoreCard()
		{
			// set the start vaules for the basic score card
			// the RowTitle will be used as a primary key incase we need to use datatable.find
			DataTable dt = this.Tables["dtScoreCard"];
			DataRow dr = dt.NewRow();
			dr["RowTitle"] = "Aces";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Twos";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Threes";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Fours";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Fives";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Sixes";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "TOTAL SCORE";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "BONUS";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "TOTAL UPPER";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "3 of a kind";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "4 of a kind";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Full House";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Small Straight";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Large Straight";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "YAHTZEE";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "Chance";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "YAHTZEE BONUS";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "TOTAL LOWER";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "TOTAL UPPER2";
			dt.Rows.Add(dr);
			dr = dt.NewRow();
			dr["RowTitle"] = "GRAND TOTAL";
			dt.Rows.Add(dr);
		}
		
		/// <summary>
		///  override the reset to leave the primary key column
		///  RowTitle intacted and set the games to dbnull
		/// </summary>
		public override void Reset()
		{
			foreach(DataRow dr in this.Tables["dtScoreCard"].Rows)
			{
				dr[1] = DBNull.Value;
				dr[2] = DBNull.Value;
				dr[3] = DBNull.Value;
				dr[4] = DBNull.Value;
				dr[5] = DBNull.Value;
			}
			
			// if the dataset is empty then it will need to have the row title filled
			if(this.Tables["dtScoreCard"].Rows.Count == 0)
				InitScoreCard();
			
		}
	}
}
