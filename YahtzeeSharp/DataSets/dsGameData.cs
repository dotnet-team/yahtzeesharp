/*************************************************************************
*                      dsGameData.cs                                     *
*                                                                        *
* Copyright (C) 2009 Andrew York <yahtzeesharp@brdstudio.net>            *
*                                                                        *
*************************************************************************/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

using System;
using System.Data;

namespace YahtzeeSharp.DataSets
{
	/// <summary>
	/// Description of dsGameData.
	/// </summary>
	public class dsGameData : DataSet
	{
		public dsGameData()
		{
			this.DataSetName = "dsGameData";
			DataTable dt = new DataTable("dtGameData");
			DataColumn[] dc = new DataColumn[] { 
												new DataColumn("colGameID",Type.GetType("System.Int32")),
												new DataColumn("colPlayerName", Type.GetType("System.String")),
												new DataColumn("colScore", Type.GetType("System.Int32")),
												new DataColumn("colDate", Type.GetType("System.DateTime")),
												new DataColumn("colIsComplete", Type.GetType("System.Boolean"))
											};
			
			dc[0].AutoIncrement = true;
			dc[0].AutoIncrementSeed = 1;
			
			dc[4].DefaultValue = false;
			
			dt.Columns.AddRange(dc);
			
			dt.PrimaryKey = new System.Data.DataColumn[] {dc[0]};
			this.Tables.Add(dt);	
		}
	}
}
